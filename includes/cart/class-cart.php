<?php
namespace HalloHalle_Onlineshop\Cart;

use HalloHalle_Onlineshop\Abstracts\Cart as AbstractCart;

use HalloHalle_Onlineshop\Cart\Session as Session;
use HalloHalle_Onlineshop\Product\Product as Product;
use HalloHalle_Onlineshop\Content\Filter as Filter;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Cart
 *
 * adds and removes products
 *
 */
class Cart {

    private $cart_session;

    private $product;

    private $filter;

    public function __construct(Session $session, Filter $filter, Product $product){

      add_action('cart_total',array($this, 'showTotal'));
      add_action('show_cart',array($this, 'showCart'));

      if(!has_action('product_total')) {
       add_action('product_total',array($this, 'showProductTotal'), 10, 1);
      }

      add_action('product_count',array($this, 'showProductCount'), 10, 1);

      $this->filter = $filter;
      $this->cart_session = $session;
      $this->product = $product;
    }

    
    /** Adds product to session cart */
    public function addProduct(){
        if(!isset($_POST['product_id'])) {
          throw new Exception('Product id missing in Cart -> addProduct()');
        }
        $product_id = $_POST['product_id'];
        $product_count = $_POST['product_count'];
        $seller_id = $_POST['seller_id'];
        //$product = $this->product->getProduct($product_id);
       return $this->cart_session->addToCart($product_id, $product_count, $seller_id);
    }


     /** Removes single productcount from session cart */
     public function removeProduct(){
      if(!isset($_POST['product_id'])) {
        throw new Exception('Product id missing in Cart -> addProduct()');
      }
      $product_id = $_POST['product_id'];
      $product_count = $_POST['product_count'];
  
      //$product = $this->product->getProduct($product_id);
      $this->cart_session->removeFromCart($product_id, $product_count);
  }


    public function showCart(){
      $this->cart_session->createPaymentObject();
      $seller = $this->cart_session->getVar('seller');
      $content = '';
      foreach($seller as $single_seller):
        $content .= '<tr><td colspan="5">'.$single_seller['name'].'</td></tr>';

        foreach ($single_seller['products'] as $product):
          $product_data = get_post($product['product_id']);
          $content .= '<tr><td><i class="icofont-close-squared-alt"></i></td>';
          $content .= '<td width="100"></td>';
          $content .=  '<td><b style="font-size:1.1em;"><b>'.$product_data->post_title.'</b></td>';
          $content .=  '<td><b style="font-size:1.1em;"><b>'.$this->filter->formatCurrency($this->getProductSinglePrice($product['product_id']),'€').'</b></td>';
          $content .=  '<td><b style="font-size:1.1em;"><b>'.$product['product_count'].'</b></td><';
          $content .=  '<td><b style="font-size:1.1em;"><b>'.$this->filter->formatCurrency($this->cart_session->getProductTotal($product['product_id']),'€').'</b></td></tr>';
        endforeach;
      endforeach;

      echo $content;
    }


    public function showTotal(){
      $value =  $this->filter->formatCurrency($this->cart_session->getTotal(),'€');
      echo $value; 

    }

    public function getTotal(){
      return $this->filter->formatCurrency($this->cart_session->getTotal(),'€');
    }

    public function getProductSinglePrice($post_id){
      return get_post_meta($post_id,'regular_price',true);
    }

    public function showProductTotal($post_id){
      echo $this->filter->formatCurrency($this->cart_session->getProductTotal($post_id),'€');
    }

    public function showProductCount($post_id){
      echo $this->cart_session->getProductCount($post_id);
    }

    public function getItemCount(){
      if(!isset($_POST['product_id'])) {
        throw new Exception('Product id missing in Cart -> getItemCount()');
      }

      return $this->cart_session->getItemCount($_POST['product_id']);
    }

    /** adds product buttons to product content, used by contentfilter */
    public function addCartButtonsToProductContent($content) {
        ob_start();
	    $content .= load_template( WP_PLUGIN_DIR.'/hallohalle_onlineshop/templates/product_to_cart.php', false, array('productid'=>1));
        $content .= ob_get_clean(); 
        return $content;  
    }
  
}
