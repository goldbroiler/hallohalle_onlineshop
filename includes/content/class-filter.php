<?php
namespace HalloHalle_Onlineshop\Content;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Class Filter
 * 
 * adds various filter functions
 */
class Filter{

    public function arrayFilterForSingleItemCount($var){
        return $var['product']->ID == $_POST['product_id'];
    }


    public static function formatCurrency($number, $label){
        return number_format($number, 2, ',', '.'). ' '.$label;
    }

}

