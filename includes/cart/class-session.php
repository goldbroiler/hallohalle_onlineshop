<?php
namespace HalloHalle_Onlineshop\Cart;
use HalloHalle_Onlineshop\Content\Filter as Filter;
use HalloHalle_Onlineshop\Payment\Payment as Payment;
use HalloHalle_Onlineshop\Product\Product as Product; 
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Session
 *
 * session handler
 *
 */
class Session {

    private $filter;

    public function __construct(Filter $filter){
        $this->filter = $filter;
    }


    /**
     * adds an object to cart
     */
    public function addToCart($product_id, $product_count = 1, $seller_id = 0){
       
        if(!$product_id) {
            throw new Exception('Product data missing in SessionHandler addToCart()');
        }

        if(!isset($_SESSION['cart'])):
            $_SESSION['cart'] = [
                'products' => []
            ];
        endif;
        
        $key = 'product_'.$product_id;

        if(!isset($_SESSION['cart']['products'][$key])):
            $product = [];
            $product['ID'] = $product_id;
            $product['count'] = $product_count;
            $product['seller'] = $seller_id;

            $_SESSION['cart']['products'][$key] = $product;

            return json_encode(array('success'=>1, 'msg' => 'Product put to cart'));
        else:
            $_SESSION['cart']['products'][$key]['count'] += $product_count;   
            return json_encode(array('success' => 1, 'msg' => 'Productcount updated (+)'));
        endif;
      
    }


    public function removeFromCart($product_id, $product_count){
        $key = 'product_'.$product_id;
        $_SESSION['cart']['products'][$key]['count'] -= $product_count;   
        echo json_encode(array('success' => 1, 'msg' => 'Productcount updated (-)'));
    }



    public function setVar($key, $value) { 
        $_SESSION[$key] = $value; 
    }

    public function getVar($key) { 
        return $_SESSION[$key]; 
    }

  
    public function getProductTotal($product_id, $count=0){
        if($count == 0){
            $key = 'product_'.$product_id;
            $count = $_SESSION['cart']['products'][$key]['count'];
        }
        $return_val = intval(get_post_meta($product_id,'regular_price',true)) * $count;
        return $return_val;
    }


    public function getProductCount($product_id){
        $key = 'product_'.$product_id;
        return $_SESSION['cart']['products'][$key]['count'] ? $_SESSION['cart']['products'][$key]['count'] : 0;
    }
    
    public function getTotal(){
        if(!isset($_SESSION['cart']['products'])){
            return 0;
        }
        $total = 0;

        foreach($_SESSION['cart']['products'] as $product):
            $total += self::getProductTotal($product['ID'], $product['count']);
        endforeach;
        
        return $total;
    }


    public function getItemCount($product_id){
        $key = 'product_'.$product_id;
        return $_SESSION['cart']['products'][$key]['count'];   
    }


    public function getPaymentObject(){
        return $this->createPaymentObject();
    }



    public function createPaymentObject(){
        $sellers_object = []; 
        if(!$_SESSION['cart']['products'] || count($_SESSION['cart']['products'])==0):
            return;
        endif;
        foreach($_SESSION['cart']['products'] as $cart_product):

            $array_key = 'seller_'.$cart_product['seller'];
            $seller = get_seller_name($cart_product['seller']);
            $seller_id = $cart_product['seller'];
            $stripe_account = get_stripe_account($cart_product['seller']);

            if(!array_key_exists($array_key, $sellers_object)):
                $sellers_object[$array_key] = array(
                    'name' => $seller,
                    'id' => $seller_id,
                    'products' => array(array('product_id' => $cart_product['ID'],'product_count' => $cart_product['count'])),
                    'payments' => array('stripe_account' => $stripe_account),
                    'total' => 0
                );
            else:
                $product_to_add = array('product_id' => $cart_product['ID'],'product_count' => $cart_product['count']);
                array_push($sellers_object[$array_key]['products'], $product_to_add);
            endif;

        endforeach;


        $_SESSION['seller'] = Payment::updateSellerTotals($sellers_object);
        return $_SESSION['seller'];

    }


    public static function setConcreteProducts(){
        $content = [];
        foreach($_SESSION['seller'] as $seller):

            $concrete_products = Product::getConcreteProducts($seller['products']);
            $_SESSION['seller']['seller_'.$seller['id']]['concrete_products'] = $concrete_products;

        endforeach;


    }

    public function deleteSession(){
        if(session_id()):
        session_unset();
        session_destroy();
        session_write_close();
    //     setcookie(session_name(),'',0,'/');
    //    session_regenerate_id(true);

        endif;
    }

}
