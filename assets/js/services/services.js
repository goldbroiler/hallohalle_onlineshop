const sendAjax = (data) => {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', ajaxurl, true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        xhr.onload = function() {
            if (xhr.status != 200) {
                let error = {
                    status: xhr.status,
                    msg: xhr.statusText
                };
                reject(error);
            } else {
                resolve(this.response);
            }
        }
        xhr.send(data);
    });

}


/** add product to cart */
const handleAddClick = (product_id, seller_id, product_count) => {

    return new Promise((resolve, reject) => {
        sendAjax('action=add_product_to_cart&product_id=' + product_id + '&product_count=' + product_count + '&seller_id=' + seller_id).then(
            data => {
                console.log(data)
                resolve(data);
            }
        ).catch(error => reject(error));
    });
}


/** remove product from cart */
const handleRemoveClick = (product_id) => {
    return new Promise((resolve, reject) => {
        const count = 1;
        sendAjax('action=remove_product_from_cart&product_id=' + product_id + '&product_count=' + count).then(
            data => {
                resolve(data);
            }
        ).catch(error => reject(error));
    });
}


const getItemCount = (product_id) => {
    return new Promise((resolve, reject) => {
        sendAjax('action=get_item_count&product_id=' + product_id).then(
            data => {
                resolve(data);
            }
        ).catch(error => reject(error));
    });
}


const getCartTotal = () => {
    return new Promise((resolve, reject) => {
        sendAjax('action=get_cart_total').then(
            data => {
                resolve(data);
            }
        ).catch(error => reject(error));
    });
}


const setItemCount = (count) => {
    document.getElementById('product_quantity').value = count;
}

const updateItemCount = (product_id) => {
    return new Promise((resolve, reject) => {
        getItemCount(product_id).then(
            data => {
                setItemCount(data);
            }
        );
    });
}


const getPreparedPaymentId = () => {
    return new Promise((resolve, reject) => {
        sendAjax('action=get_prepared_payment_id').then(
            data => {
                console.log(data)
                if (data == 0) {
                    reject('No Payment Object')
                }
                resolve(data);
            }
        ).catch(error => reject(error));
    });
}


const deleteSession = () => {
    return new Promise((resolve, reject) => {
        sendAjax('action=delete_session').then(
            data => {
                resolve(data);
            }
        ).catch(error => reject(error));
    });
}

const checkURL = () => {
    if (getUrlVars()["payment_succeeded"]) {
        deleteSession().then(
            data => console.log('session deleted')
        )
    }
}


const getUrlVars = () => {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}


const getOptions = () => {
    return new Promise((resolve, reject) => {
        sendAjax('action=get_options').then(
            data => {
                resolve(data);
            }
        ).catch(error => reject(error));
    });
}


const handlePayment = () => {
    return new Promise((resolve, reject) => {
        sendAjax('action=handle_payment').then(
            data => {
                resolve(data);
            }
        ).catch(error => reject(error));
    });
}
export { getCartTotal, getItemCount, handleAddClick, handleRemoveClick, getPreparedPaymentId, getOptions, checkURL, handlePayment }