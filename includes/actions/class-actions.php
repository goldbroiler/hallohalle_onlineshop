<?php
namespace HalloHalle_Onlineshop\Actions;
use HalloHalle_Onlineshop\Abstracts\Actions as AbstractActions;
use HalloHalle_Onlineshop\Cart\Cart as Cart;
// use HalloHalle_Onlineshop\Templates\Templates as Templates;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Class Actions
 * 
 * adds actions
 * uses actions-methods from other classes
 */
class Actions extends AbstractActions {

    /** adds an axction, which shows the onlibe purchasable products in cart view */
    public function addActionShowOnlineProductsInCartView(){
        $cart = new Cart();
        add_action('onlineproducts', array($cart,'getOnlineProductsForCartView'));

       // add
    }


}
