'use strict'

import { getPreparedPaymentId, checkURL, getOptions, handlePayment } from './services/services.js';

const buttons = {
    paynow: 'paynow',
}

//will contain a json array of option 
var options;


const stripe = Stripe('pk_test_51HAsLFFrOhMwejIpPLdtLjGXijN5maq8tlsq4m09vq4PYubYLRvAcdDxD0qlPUiJ7mltsd1nq3xW820WCatocBo200VzdVrKhe');
var elements;

///checkURL();

if (document.getElementById('payment-element')) {

    getOptions().then(
        data => {
            options = JSON.parse(data);
        }
    ).catch(error => console.log(error));

    getPreparedPaymentId().then(data => {
        elements = stripe.elements({ clientSecret: data, locale: 'de' });
        var paymentElement = elements.create('payment', {
            fields: {
                billingDetails: {
                    name: 'auto',
                    email: 'auto',
                }
            }
        });


        paymentElement.mount('#payment-element');
    }).catch(
        error => {
            var css = "color:lightgreen";
            console.log("%cPayment:::%s", css, 'no current payment object')
        }
    );
}


/** Use event delegation to bind click event to ajax loaded productpage */
document.addEventListener('click', e => {

    //  Delegate event to add product button
    if (e.target.getAttribute('id') == buttons.paynow) {

        e.preventDefault();

        handlePayment().then(
            data => {


                stripe.confirmPayment({
                        elements,
                        confirmParams: {
                            // payment_intent=pi_3KNuDfFrOhMwejIp1Is9A0Lw&payment_intent_client_secret=pi_3KNuDfFrOhMwejIp1Is9A0Lw_secret_QzMvDDTXNRhZyhOSSwoMaxrH4&redirect_status=failed&source_type=giropay
                            return_url: data,
                        },
                    })
                    .then(function(result) {
                        if (result.error) {}
                    });


            }
        )



    }

}, true);