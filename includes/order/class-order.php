<?php
namespace HalloHalle_Onlineshop\Order;
use HalloHalle_Onlineshop\Cart\Session as Session;
use HalloHalle_Onlineshop\Content\Filter as Filter;
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Order {

  public static function getOrderTotal($products){
		$content = 0;
		
		foreach($products as $product){
			$content += $product['product_total'];
		}
	
		return $content;
  }	




	public static function getProductsObject($products){
		$content = '';
		
		foreach($products as $product){
			$content .= "<h4>".$product['product_title']."</h4>";
			$content .= "<p>".Filter::formatCurrency($product['product_price'],'€')."</p>";
			$content .= "<h5><u>".Filter::formatCurrency($product['product_total'],'€')."</u></h5>";
		}

		return $content;
	}
	


	
	/**
	 * create order object for wp_insert_post
	 * (set ID means, the post gets just updated)
	 * @return WP_POST array
	 */
	public function getOrderObject(){

		$seller = Session::getVar('seller');
		$total = Filter::formatCurrency(Session::getTotal(),'€');


		$content = '';
		$title = '';

		foreach($seller as $single_seller){
			$title .= $single_seller['name'].' ';
			$content .= "<h2>".$single_seller['name']."</h2>";
			$content .= "<p>".self::getProductsObject($single_seller['concrete_products'])."</p><hr>";
		}
		
		$content .= "<p><hr></p><p><h4>".$total."</h4></p>";


		if(Session::getVar('order_id') && Session::getVar('order_id')>0):
			$post= array(
				'ID' => Session::getVar('order_id'),
				'post_title'    => 'Bestellung '.Session::getVar('order_id'),
				'post_content'  => $content,
				'post_type' => 'shoporder',
				'post_status'   => 'publish',
				'post_author'   => 1,
			);
		else:
			$post= array(
				'post_title'    => 'Bestellung '.wp_strip_all_tags($title),
				'post_content'  => $content,
				'post_type' => 'shoporder',
				'post_status'   => 'publish',
				'post_author'   => 1,
			);
		endif;

		return $post;
	}


	/** adds invoices array to order
	 * @param serialized array
	*/
	public static function updateOrderInvoices($invoices, $order){
		return update_post_meta($order,'invoices', $invoices);
	}	

	/** adds sellers array to order
	 * @param serialized array
	*/
	public static function updateOrderSellers($sellers, $order){
		return update_post_meta($order,'sellers', $sellers);
	}
	
	/** update order status
	 * @param string unpaid|paid
	*/
	public static function updateOrderStatus($status, $order){
		return update_post_meta($order,'status', $status);
	}	
}
