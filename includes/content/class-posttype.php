<?php
namespace HalloHalle_Onlineshop\Content;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Class Filter
 * 
 * adds various filter functions
 */
class Posttype{

    public $textdomain = 'hallohalle_onlineshop';

    public function __construct(){
        $this->addPostType();
    }

    public function addPostType(){
        if(!post_type_exists('product')):
            $this->registerType('product','Produkte', 'Produkt');
        endif;    

        if(!post_type_exists('shoporder')):
            $this->registerType('shoporder','Bestellungen', 'Bestellung');
        endif;  

        if(!post_type_exists('customer')):
            $this->registerType('customer','Kunden', 'Kunde');
        endif;  
    }
    
    public function registerType($type, $plural, $singular) {
       
        register_post_type($type,
            array(
                'labels' => 
                array(
                    'name'          => __($plural, $this->textdomain),
                    'singular_name' => __($singular, $this->textdomain)
                ),
                'public'      => true,
                'has_archive' => false,
                'supports' => array('title', 'editor', 'thumbnail')
            )
        );
    }
}
