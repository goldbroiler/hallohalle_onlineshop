<?php
namespace HalloHalle_Onlineshop\Filter;
use HalloHalle_Onlineshop\Abstracts\Contentfilter as AbstractContentfilter;
use HalloHalle_Onlineshop\Templates\Templates as Templates;


// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Contentfilter extends AbstractContentfilter{

    public $content_type = 'product';


    /** adds filter to add and remove product buttons to product content */
    public function addFilterGetButtons(){
       
        $template = new Templates();
      
        $current_content_type = get_post_type(get_the_ID());

        if($current_content_type === $this->content_type):
          add_filter( 'the_content', array($template,'getCartButtonsView'));
        endif;  


        if(is_page('payment')):
          add_filter( 'the_content', array($template,'afterPaymentActions'));
        endif;  
    }


     /** adds js  */
     public static function addScripts(){
       $handler = new self();
       add_filter('script_loader_tag', array($handler,'addModuleTypeToScriptTag'), 10, 3); //import as module
       wp_enqueue_script( 'hallohalle_onlineshop_stripe_elements', 'https://js.stripe.com/v3/', '', '1.0.0', false);
       wp_enqueue_script( 'hallohalle_onlineshop_cart', '/wp-content/plugins/hallohalle_onlineshop/assets/js/cart.js', '', '1.0.0', true);
       wp_enqueue_script( 'hallohalle_onlineshop_checkout', '/wp-content/plugins/hallohalle_onlineshop/assets/js/checkout.js', '', '1.0.0', true);
       wp_enqueue_style( 'hallohalle_onlineshop_fontawesome', '/wp-content/plugins/hallohalle_onlineshop/assets/css/fontawesome-free-5.14.0-web/css/all.css', '', '1.0.0', false);
     }

   

    public function addModuleTypeToScriptTag( $tag, $handle, $src ) {
      if ( 'hallohalle_onlineshop_cart' === $handle || 'hallohalle_onlineshop_checkout' === $handle) {
          $tag = '<script type="module" src="' . esc_url( $src ) . '" id="hallohalle_onlineshop_cart" defer></script>';
      }
   
      return $tag;
  }
 
    
  
}
