<?php
namespace HalloHalle_Onlineshop\Content;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Class Metafields
 * 
 * adds meta fields to custom  post types
 */
class Metafields{

    public $textdomain = 'hallohalle_onlineshop';


    public function __construct(){
        add_action( 'add_meta_boxes', array( $this, 'addProductFieldsMetabox' ) );
        add_action( 'save_post',      array( $this, 'save' ) );
    }


    public function addProductFieldsMetabox() {
        add_meta_box(
            'productfields', 
            'Produktdaten',
             array($this,'productfieldsCallback'), 
            'product', 
            'normal', 
            'high' 
        );
    }


    protected function addField($postid, $name, $type, $label) {
      $value = get_post_meta( $postid, $name, true );

      $return_label = '<label for="'.$name.'">'._e( $label, $this->textdomain ).'</label>';
      $return_input = '<div><input type="'.$type.'" id="'.$name.'" name="'.$name.'" value="'.esc_attr( $value ).'" size="25" /></div><hr/>';

      if($type === 'checkbox'):
        $checked = '';
        if($value == 1): $checked=' checked '; endif;
        $return_input = '<div><input type="'.$type.'" id="'.$name.'" '.$checked.' onChange="this.checked ? this.value=1 : this.value=0" name="'.$name.'" value="'.esc_attr( $value ).'" size="25" /></div><hr/>';
      endif;

      echo $return_label.$return_input;
    }


     /**
     * Render Meta Box content.
     *
     * @param WP_Post $post The post object.
     */
    public function productfieldsCallback( $post ) {
        // Add an nonce field so we can check for it later.
        wp_nonce_field( 'hallohalle_onlineshop_custom_box', 'hallohalle_onlineshop_custom_box_nonce' );
    
        $this->addField($post->ID, 'regular_price', 'text', 'Produktpreis');
        $this->addField($post->ID, 'regular_tax', 'text', 'Aktuell gültige Mwst:');
        $this->addField($post->ID, 'stock', 'number', 'Anzahl am Lager');
        $this->addField($post->ID, 'online_purchasable', 'checkbox', 'Online zahlbar?');
        $this->addField($post->ID, 'seller', 'text', 'Verkäufer');
        $this->addField($post->ID, 'stripe_account', 'text', 'Stripe Account');
    }




    /**
     * Save the meta when the post is saved.
     *
     * @param int $post_id The ID of the post being saved.
     */
    public function save( $post_id ) {
 
        /*
         * We need to verify this came from the our screen and with proper authorization,
         * because save_post can be triggered at other times.
         */
      
        // Check if our nonce is set.
        if ( ! isset( $_POST['hallohalle_onlineshop_custom_box_nonce'] ) ) {
            return $post_id;
        }
 
        $nonce = $_POST['hallohalle_onlineshop_custom_box_nonce'];
 
        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce, 'hallohalle_onlineshop_custom_box' ) ) {
            return $post_id;
        }
 
        /*
         * If this is an autosave, our form has not been submitted,
         * so we don't want to do anything.
         */
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }
 
        // Check the user's permissions.
        if ( 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }
        
        
        
        update_post_meta( $post_id, 'regular_price', sanitize_text_field($_POST['regular_price']) );
        update_post_meta( $post_id, 'regular_tax', sanitize_text_field($_POST['regular_tax']) );
        update_post_meta( $post_id, 'stock', sanitize_text_field($_POST['stock']) );
        update_post_meta( $post_id, 'online_purchasable', $_POST['online_purchasable'] );
        update_post_meta( $post_id, 'seller', sanitize_text_field($_POST['seller']) );
        update_post_meta( $post_id, 'stripe_account', sanitize_text_field($_POST['stripe_account']) );
    }
     
}
