<?php
namespace HalloHalle_Onlineshop\DI;
use DI\Container as Container;


// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Di {

    public $container;

    public function __construct(){
        $this->container = new Container();
    }

    public function createInstances(){
        $session = $this->container->get('HalloHalle_Onlineshop\Cart\Session');
        $product = $this->container->get('HalloHalle_Onlineshop\Product\Product');
        $filter = $this->container->get('HalloHalle_Onlineshop\Content\Filter');
        $cart = $this->container->get('HalloHalle_Onlineshop\Cart\Cart');
        $posttype = $this->container->get('HalloHalle_Onlineshop\Content\Posttype');
        $metafields = $this->container->get('HalloHalle_Onlineshop\Content\Metafields');
       // $cart = new Cart($session, $filter, $product);
      // add ajax callbacks
        $xhr_callbacks = $this->container->get('HalloHalle_Onlineshop\Xhr\Xhr');
        $xhr_api = $this->container->get('HalloHalle_Onlineshop\Xhr\XhrApi');
        $xhr_callbacks->addAjaxCallbacks($xhr_api); 
       // $xhr_api = new XhrApi($cart);
     }
}