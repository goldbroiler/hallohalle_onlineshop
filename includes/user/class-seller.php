<?php
namespace HalloHalle_Onlineshop\User;
use HalloHalle_Onlineshop\Cart\Session as Session;
require_once(dirname(dirname(dirname(__FILE__))).'/lib/pdf/vendor/autoload.php');

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Seller {

    public static function getInvoiceNumber($seller_id){
        $current_invoice_number = get_user_meta($seller_id,'invoice_number',true);

        if(!$current_invoice_number): //first invoice
            $new_invoice_number = 1;
        else:
            $new_invoice_number = $current_invoice_number + 1;
        endif;

        update_user_meta($seller_id,'invoice_number',$new_invoice_number);
        return $new_invoice_number;
    }
  
}
