<?php
namespace HalloHalle_Onlineshop\Product;
use HalloHalle_Onlineshop\Abstracts\Product as AbstractProduct;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Product
 * 
 * provides product functionality
 * 
 */
class Product extends AbstractProduct{

	/** 
	 * concrete implementation with get_post from wordpress 
	 * @return array $resul[online => 1|0, product => WP Post Obj]
	 * */

	public function getProduct($product_id){
		if(!$product_id):
			throw new Exception('Product ID missing in ProductClass getProduct()');
		endif;

		$product = get_post($product_id);
		
		$result = [
			'online' => get_post_meta($product_id, 'online_zahlen', true) === 1 ? 1 : 0,
			'product' => $product
		];

		return $result;
	}

	public static function getProductPrice($product_id){
		return get_post_meta($product_id,'regular_price', true);
	}


	//Returns real prodzcts with titels, totals and text
	public static function getConcreteProducts($products){
	
		$concrete_products = [];
	
		foreach($products as $product):
			$concrete_product = [];
			$product_post = get_post($product['product_id']);
		
			$concrete_product['product_title'] = $product_post->post_title.' ('.$product['product_count'].')';
			$concrete_product['product_price'] = number_format(get_post_meta($product['product_id'],'regular_price',true),2,',','.');
			$concrete_product['product_total'] = number_format( ($product['product_count'] *get_post_meta($product['product_id'],'regular_price',true)),2,',','.');
		
			array_push($concrete_products, $concrete_product);
		endforeach;
		
		return $concrete_products;
	}
	

}

