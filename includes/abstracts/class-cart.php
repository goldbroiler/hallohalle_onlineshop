<?php
namespace HalloHalle_Onlineshop\Abstracts;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Cart
 *
 * adds and removes products
 *
 */
abstract class Cart {


    abstract public function getSession();

    abstract public function getProduct();

    abstract public function addProduct();

    abstract public function removeProduct();

    abstract public function showCart();

    abstract public function addCartButtonsToProductContent($content);
  
}
