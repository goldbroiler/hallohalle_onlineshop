<?php
namespace HalloHalle_Onlineshop\Invoices;
use HalloHalle_Onlineshop\Cart\Session as Session;
use HalloHalle_Onlineshop\User\Seller as Seller;

require_once(dirname(dirname(dirname(__FILE__))).'/lib/pdf/vendor/autoload.php');

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Invoice {

	public static $path;

	public function __construct(){
		self::$path = get_bloginfo('url').'/wp-content/plugins/hallohalle_onlineshop';
	}

	public static function getSellerLogo(){
		return '<td style="width:30%"><img src="'.self::$path.'/assets/images/logo-seller.png"></td>';
	}

	public static function getLogo(){
		return '<td><img src="'.self::$path.'/assets/images/logo.svg"></td>';
	}

	public static function getEmptyTd($percent){
		return '<td style="width:'.$percent.'%">&nbsp;</td>';
	}


	public static function getHTMLHead(){
		$html = '<table><tr>';
		$html .= self::getSellerLogo().self::getEmptyTd('40').self::getLogo();
		$html .=  '</tr></table>';
		return $html;
	}

	/** creates invoices 
	 * 
	 * @return array of all created invoices
	 */
    public static function addInvoices(){

		if( Session::getVar('invoices')): 
			return; 
		endif;
		
		$invoices = [];

		$seller = Session::getVar('seller');
		foreach($seller as $single_seller):
			$pdfname = strtolower(substr($single_seller['name'],6)).'_order-'.Session::getVar('order_id').'_invoice-'.Seller::getInvoiceNumber($single_seller['id']);
			$mpdf = new \Mpdf\Mpdf();
			$html = self::getHTMLHead();
			$mpdf->WriteHTML($html);
			$mpdf->Output( dirname(dirname(dirname(__FILE__))).'/lib/pdf/invoices/'.$pdfname.'.pdf','F' );
			array_push($invoices, $path.'/lib/pdf/invoices/'.$pdfname);
			Session::setVar('invoices', 'true');
		endforeach;

		return $invoices;
	}
  
}
