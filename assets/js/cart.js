'use strict'

import { handleAddClick, handleRemoveClick, getCartTotal, getItemCount } from './services/services.js';

const buttons = {
    add: 'btn_add_product',
    remove: 'btn_remove_product',
}

/** Use event delegation to bind click event to ajax loaded productpage */
document.addEventListener('click', e => {

    //  Delegate event to add product button
    if (e.target.parentElement.getAttribute('id') == buttons.add) {
        const product_id = e.target.parentElement.getAttribute('data-product-id');
        const product_count = document.getElementById('product_quantity').value;
        const seller_id = e.target.parentElement.getAttribute('data-seller-id');

        handleAddClick(product_id, seller_id, product_count).then(

            data => {
                cartTotal();
                itemCount(product_id);
            }
        ).catch(
            error => {
                throw new Error(error.msg);
            }
        );
    }


    //  Delegate event to remove product button
    if (e.target.parentElement.getAttribute('id') == buttons.remove) {
        const product_id = e.target.parentElement.getAttribute('data-product-id');

        handleRemoveClick(product_id).then(
            data => {
                cartTotal();
                itemCount(product_id);
            }
        ).catch(
            error => {
                throw new Error(error.msg);
            }
        );
    }

}, true);







document.addEventListener('keydown', e => {

    if (e.code === 'Enter' || e.code === 'NumpadEnter') {

        //  Delegate event to add product button
        if (document.activeElement.getAttribute('id') == 'product_quantity') {
            const product_id = e.target.parentElement.getAttribute('data-product-id');
            const product_count = e.target.value;
            const seller_id = e.target.parentElement.getAttribute('data-seller-id');

            handleAddClick(product_id, seller_id, product_count).then(
                data => {
                    cartTotal();
                    itemCount(product_id);
                }
            ).catch(
                error => {
                    throw new Error(error.msg);
                }
            );
        }
    }
}, true)


const itemCount = (product_id) => {
    getItemCount(product_id).then(
        data => {
            console.log(product_id);
            document.getElementById('singlecount').innerHTML = data;
        }
    ).catch(
        error => {
            throw new Error(error.msg);
        }
    );
}

const cartTotal = () => {
    getCartTotal().then(
        data => {
            document.getElementById('cart_total').innerHTML = data;

        }
    ).catch(
        error => {
            throw new Error(error.msg);
        }
    );
}