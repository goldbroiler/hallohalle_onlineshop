<?php
namespace HalloHalle_Onlineshop\Admin;


// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Options
 *
 * adds plugin options
 *
 */
class Options {

    public function __construct(){
        add_action( 'admin_menu', array($this,'addMenuPage'));
        add_action( 'admin_init', array($this,'registerSettings'));
    }



    public function registerSettings(){

        register_setting( 
            'onlineshop', 
            'stripe_api_key_open'
        );

        register_setting( 
            'onlineshop', 
            'stripe_api_key_secret'
        );
      

        register_setting( 
            'onlineshop', 
            'stripe_api_mode'
        );
    
        
        add_settings_section(
            'stripe_options_section',
            __( 'Stripe Einstellungen', 'hallohalle_onlineshop' ), 
            array($this,'showSectionInfo'),
            'onlineshop'
        );

 
        add_settings_field(
            'stripe_api_key_pk', 
            __( 'Stripe Open Apikey', 'hallohalle_onlineshop' ),
            array($this, 'getFormInput'),
            'onlineshop',
            'stripe_options_section',
            array('option'=>'stripe_api_key_open')
        );


        add_settings_field(
            'stripe_api_key_sk', 
            __( 'Stripe Secret Apikey', 'hallohalle_onlineshop' ),
            array($this, 'getFormInput'),
            'onlineshop',
            'stripe_options_section',
            array('option'=>'stripe_api_key_secret')
        );

        add_settings_field(
            'stripe_api_mode', 
            __( 'Stripe Api Modus', 'hallohalle_onlineshop' ),
            array($this, 'getFormInput'),
            'onlineshop',
            'stripe_options_section',
            array('option'=>'stripe_api_mode')
        );
    }


    public function getFormInput( $args ) {
          // get the value of the setting we've registered with register_setting()
            $setting = get_option($args['option']);
            // output the field
            ?>
            <input type="text" name="<?php echo $args['option']; ?>" value="<?php echo isset( $setting ) ? esc_attr( $setting ) : ''; ?>">
            <?php
    }
    public function getFormInput2( $args ) {
        // get the value of the setting we've registered with register_setting()
          $setting = get_option('stripe_options');
          // output the field
          ?>
          <input type="text" name="stripe_options" value="<?php echo isset( $setting ) ? esc_attr( $setting ) : ''; ?>">
          <?php
  }
  public function getFormInput3( $args ) {
    // get the value of the setting we've registered with register_setting()
      $setting = get_option('stripe_options');
      // output the field
      ?>
      <input type="text" name="stripe_options" value="<?php echo isset( $setting ) ? esc_attr( $setting ) : ''; ?>">
      <?php
}

        public function showSectionInfo( $args ) {
            ?>
            <p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( 'Follow the white rabbit.', 'wporg' ); ?></p>
            <?php
        }



    /**
     * Add the top level menu page.
     */
    public function addMenuPage() {
        add_menu_page(
            'Onlineshop',
            'Onlineshop',
            'manage_options',
            'onlineshop', //meu slug
            array($this,'showPage')
        );
    }


    /**
     * Top level menu callback function
     */
    public function showPage() {

        // check user capabilities
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }
    
        update_option( 'stripe_api_key', $_POST['stripe_api_key'], true );

        if ( isset( $_GET['settings-updated'] ) ) {
            // add settings saved message with the class of "updated"
            add_settings_error( 'wporg_messages', 'wporg_message', __( 'Settings Saved', 'wporg' ), 'updated' );
        }
    
        // show error/update messages
        settings_errors( 'wporg_messages' );
        ?>
        <div class="wrap">
            <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
            <form action="options.php" method="post">
                <?php
                // output security fields for the registered setting "wporg"
                settings_fields( 'onlineshop' );
                // output setting sections and their fields
                // (sections are registered for "wporg", each field is registered to a specific section)
                do_settings_sections( 'onlineshop' );
                // output save settings button
                submit_button( 'Save Settings' );
                ?>
            </form>
        </div>
        <?php
    }


    public static function getOption($option){
        return get_option($option);
    }

}