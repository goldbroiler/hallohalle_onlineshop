
<div id="hh_cart">
    <div id="hh_inputs" data-seller-id="<?php echo get_seller($post->ID); ?>" data-product-id="<?php echo $post->ID; ?>">
        <input type="text" class="input-text qty text" value="1" id="product_quantity">
    </div>
    <div id="hh_buttons">
        <button class="addtobasketbtn" id="btn_remove_product" data-seller-id="<?php echo get_seller($post->ID); ?>" data-product-id="<?php echo $post->ID; ?>">
            <i class="fas fa-minus-circle"></i>
        </button>
        <button class="addtobasketbtn"  id="btn_add_product"  data-seller-id="<?php echo get_seller($post->ID); ?>" data-product-id="<?php echo $post->ID; ?>">
            <i class="fas fa-plus-circle"></i>
        </button>
    </div>
    <div id="hh_cart_spinner" style="display: none;">
            <div class="spinner-grow" role="status">
                <span class="sr-only">Loading...</span>
            </div>
    </div>
</div>
<div id="hh_summary">

<h3>Gesamtpreis Warenkorb: <span class="cartprice hh_totals" id="cart_total" style="opacity: 1;"><?php echo do_action('cart_total'); ?></h3>

<p>Davon <span id="singlecount"><?php do_action('product_count', $post->ID); ?></span>  mal <?php the_title(); ?></p>
</div>
