<?php
namespace HalloHalle_Onlineshop\Shortcodes;
use HalloHalle_Onlineshop\Abstracts\Shortcodes as AbstractShortcodes;

use HalloHalle_Onlineshop\Payment\Payment as Payment;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Class Shortcodes
 * 
 * adds shortcodes
 * uses shortcode-methods from other classes
 */
class Shortcodes extends AbstractShortcodes {

    public function __construct(){
        $this->addShortcodes();
    }

    /** adds shortcode to display the product buttons like add, remove e.g. */
    public static function addCartShortcode(){
        add_shortcode('cart', array('HalloHalle_Onlineshop\Templates\Templates','getCartView'));
    }

     /** adds shortcode to display the product buttons like add, remove e.g. */
    public static function addCheckoutShortcode(){
        add_shortcode('checkout', array('HalloHalle_Onlineshop\Templates\Templates','getCheckoutView'));
    }


    public function addShortcodes(){
        $this->addCartShortcode();
        $this->addCheckoutShortcode();
    }
}
