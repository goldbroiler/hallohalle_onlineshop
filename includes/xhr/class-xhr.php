<?php
namespace HalloHalle_Onlineshop\Xhr;
use HalloHalle_Onlineshop\Xhr\XhrApi as XhrApi;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Xhr
 *
 * adds ajax actions to plugin
 * offers api to shop 
 */
class Xhr {

    public function addAjaxCallbacks($xhr_api){
     
        add_action( 'wp_ajax_add_product_to_cart', array( $xhr_api, 'addProduct' ) );
        add_action( 'wp_ajax_nopriv_add_product_to_cart',  array( $xhr_api, 'addProduct' ) );   

        add_action( 'wp_ajax_remove_product_from_cart', array( $xhr_api, 'removeProduct' ) );
        add_action( 'wp_ajax_nopriv_remove_product_from_cart',  array( $xhr_api, 'removeProduct' ) );   

        add_action( 'wp_ajax_get_item_count', array( $xhr_api, 'getItemCount' ) );
        add_action( 'wp_ajax_nopriv_get_item_count',  array( $xhr_api, 'getItemCount' ) );

        add_action( 'wp_ajax_get_cart_total', array( $xhr_api, 'getCartTotal' ) );
        add_action( 'wp_ajax_nopriv_get_cart_total',  array( $xhr_api, 'getCartTotal' ) );

        add_action( 'wp_ajax_get_prepared_payment_id', array( $xhr_api, 'getPreparedPaymentId' ) );
        add_action( 'wp_ajax_nopriv_get_prepared_payment_id',  array( $xhr_api, 'getPreparedPaymentId' ) );

        add_action( 'wp_ajax_delete_session', array( $xhr_api, 'deleteSession' ) );
        add_action( 'wp_ajax_nopriv_delete_session',  array( $xhr_api, 'deleteSession' ) );

        add_action( 'wp_ajax_get_options', array( $xhr_api, 'getOptions' ) );
        add_action( 'wp_ajax_nopriv_get_options',  array( $xhr_api, 'getOptions' ) );

        add_action( 'wp_ajax_handle_payment', array( $xhr_api, 'handlePayment' ) );
        add_action( 'wp_ajax_nopriv_handle_payment',  array( $xhr_api, 'handlePayment' ) );
        
    }

  
}
