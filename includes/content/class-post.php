<?php
namespace HalloHalle_Onlineshop\Content;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Payment Factory
 *
 *
 */
class Post {

    public static function createPost($content, $type){
        // Create post object
        $post = $content;
        
        // Insert the post into the database
       return wp_insert_post( $post );
    }

}