<?php
namespace HalloHalle_Onlineshop\Templates;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Templates {

  public static $itemspage = 'events';

  static function setup_actions() {
     $handler = new self();
     add_filter( 'template_include', array( $handler, 'custom_templates') );
   } 
  
  
  
  /*
  *
  * Loads custom template file
  * 
  */
 
  
  public static function custom_templates( $template ) {
    if(is_page('onlineshop')):
        return dirname(dirname(dirname( __FILE__ ))) . '/templates/onlineshop.php';
    else:
      return $template;    
    endif; 
  }


  /** adds product buttons to product content, used by contentfilter */
  public function getCartButtonsView() {
    ob_start();
    $content .= load_template( WP_PLUGIN_DIR.'/hallohalle_onlineshop/templates/product_to_cart.php', false, array('productid'=>1));
    $content .= ob_get_clean(); 
    return $content;  
  }


  /** get template for cart, also used by shortcode [cart] */
  public function getCartView(){
    ob_start();

    $content .= load_template( WP_PLUGIN_DIR.'/hallohalle_onlineshop/templates/cart.php', false, array());
    $content .= ob_get_clean(); 
    return $content;  
  }


    /** get template for checkout, also used by shortcode [checkout] */
    public function getCheckoutView(){
      ob_start();
      $content .= load_template( WP_PLUGIN_DIR.'/hallohalle_onlineshop/templates/checkout.php', false, array());
      $content .= ob_get_clean(); 
      return $content;  
    }


    /** get template for checkout, also used by shortcode [checkout] */
    public function afterPaymentActions(){
      ob_start();
      $content .= load_template( WP_PLUGIN_DIR.'/hallohalle_onlineshop/templates/payment.php', false, array());
      $content .= ob_get_clean(); 
      return $content;  
    }
  
}
