<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the
 * plugin admin area. This file also includes all of the dependencies used by
 * the plugin, registers the activation and deactivation functions, and defines
 * a function that starts the plugin.
 *
 *
 * @wordpress-plugin
 * Plugin Name:       HalloHalle Onlineshop
 * Plugin URI:        https://hallohalle.de/deals-storys
 * Description:       Onlineshop functionality for hallohalle.de
 * Version:           1.0.0
 * Author:            Björn Zschernack
 * Author URI:        https://dergoldbroiler.de
 * License:           MIT
 */


// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}




// Load the autoloader from it's own file
require_once plugin_dir_path( __FILE__ ) . 'autoload.php';

// Load the DI autoloader
require_once plugin_dir_path( __FILE__ ) . 'lib/php-di/vendor/autoload.php';

$dependencies = new HalloHalle_Onlineshop\DI\Di();

add_action('init', array($dependencies,'createInstances'));



// start session, if necessary
function initShopSession() {
   if ( ! session_id() ) {
       session_start();
   }
}
add_action( 'init', 'initShopSession' );
 





// setup some initial stuff
function setupShop(){
   $content_filter = new HalloHalle_Onlineshop\Filter\Contentfilter();
   // add all shortcodes
   $shortcodes = new HalloHalle_Onlineshop\Shortcodes\Shortcodes();
   //$shortcodes->addShortcodes();
   
   // filter to add sgop stuff the product content
   $content_filter->addFilterGetButtons(); 
   
}
add_action('the_post','setupShop');

HalloHalle_Onlineshop\Filter\Contentfilter::addScripts();






function ajaxurl() {
   echo '<script type="text/javascript">var ajaxurl = "'.get_bloginfo('url').'/wp-admin/admin-ajax.php" </script>';
}
// Add hook for front-end <head></head>
add_action( 'wp_head', 'ajaxurl' );


/** some functions */
function get_seller($postid){
   $seller = get_post_meta($postid,'seller',true);
   return $seller;
}

function get_seller_name($userid){
   return get_user_meta($userid, 'first_name', true).' '.get_user_meta($userid, 'last_name', true);
}

function get_stripe_account($userid){
   return get_user_meta($userid, 'stripe_account', true);
 }
?>