<?php
namespace HalloHalle_Onlineshop\Abstracts;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

abstract class Contentfilter {

    public $content_type = 'product';
    public $cart;


    /** adds filter to add and remove product buttons to product content */
    abstract public function addFilterGetButtons();

  
}
