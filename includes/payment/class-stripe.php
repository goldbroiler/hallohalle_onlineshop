<?php
namespace HalloHalle_Onlineshop\Payment;
use HalloHalle_Onlineshop\Admin\Options as Options;
use HalloHalle_Onlineshop\Cart\Session as Session;

require_once(WP_PLUGIN_DIR.'/hallohalle_onlineshop/lib/stripe/vendor/autoload.php');

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Payment Factory
 *
 *
 */
class Stripe {

    public $mode;

    public $options;

    public $stripe;

    public $session;

    public function __construct(Session $session, Options $options){
       $this->options = $options;
       $this->session = $session;
     
        $this->mode = $this->options->getOption('stripe_api_mode');

        $this->stripe = new \Stripe\StripeClient($this->options->getOption('stripe_api_key_secret') );
    }

    /**
     * creates / updates paymentintent and
     * saves whole intent object to session 
     */
    public function preparePayment(){

        $this->payment_object = $this->session->getPaymentObject();

        if(!$this->payment_object){
            return;
        }

        $payment_amount = $this->getFullIntentAmount();

        $prepared_payment = $this->session->getVar('prepared_payment');
       
        $stripe_accounts = $this->getStripeAccounts();

        if(!$prepared_payment):

            /** create external payment */
            $payment_intent = $this->stripe->paymentIntents->create([
                'amount' => $payment_amount,
                'currency' => 'eur',
                'payment_method_types' => ['card','sofort','giropay','klarna'],
                'transfer_group' => time(),
                'metadata' => [
                    'stripe' => $stripe_accounts ,
                ]  
                
            ]);

        else:

            /** update external payment */
            $payment_intent = $this->stripe->paymentIntents->update(
                $prepared_payment, [
                'amount' => $payment_amount,
                'currency' => 'eur',
                'payment_method_types' => ['card','sofort','giropay','klarna'],
                'metadata' => [
                    'stripe' => $stripe_accounts ,
                ]  
        
                
            ]);
            
        endif;
       
        if($payment_intent['id']) {
            $this->session->setVar('prepared_payment',$payment_intent['id']); 
        } else {
            return false;
        }
    }

    /** calculates the integer needed by stripe  (112,12 * 100 = 11212)
    * @return integer
    */
    public function getFullIntentAmount(){
        return $this->session->getTotal() * 100;
    }



    /** get stripe accounts formatted as stripe metadata 
     * @return json_encoded_array
    */
    public function getStripeAccounts(){
        $metadata = array();
        if($this->payment_object){
            foreach($this->payment_object as $single_row):
                array_push($metadata,$single_row['payments']['stripe_account'].'#'.($single_row['total']*100));
            endforeach;
        }
        return json_encode($metadata);
    }


    /** get stripe accounts formatted as stripe metadata 
    * @return String (Stripe Client Secret) 
    */
    public function getPreparedPaymentId(){
      if($this->session->getVar('prepared_payment')){
        $payment_intent = $this->stripe->paymentIntents->retrieve($this->session->getVar('prepared_payment'));
        return $payment_intent['client_secret'];
        }

        return 0;

    }

}


/*
acct_1JkVCa2EX3L2C4nC


acct_1HWjExF7S21snJOQ
  public function createIntent($echo=true){
    if(!$_SESSION['stripe']):
      $_SESSION['stripe'] = returnStripe(get_field('stripemode','options'));   
    endif;
    $accounts = Plugin_Stripe_Checkout::getStripeAccountsForMetadata();
    if($_SESSION['list'] && $_SESSION['stripe_accounts']):
    
      $full_intent_amount = 0;
      foreach($_SESSION['stripe_accounts'] as $acc):
        $full_intent_amount += $acc;
      endforeach;
      
      if($full_intent_amount > 0):
    
        if(!$_SESSION['payment_intent']):
          $_SESSION['payment_intent'] = $_SESSION['stripe']->paymentIntents->create([
            'amount' => $full_intent_amount,
            'currency' => 'eur',
            'payment_method_types' => ['card','sofort'],
            'transfer_group' => time(),
          
          ]);
        else:
          $products = array();
          foreach($_SESSION['list'] as $item){
            array_push($products,$item->product_id.':::'.$item->product_count);
          }

         $intent=$_SESSION['stripe']->paymentIntents->update(
          $_SESSION['payment_intent']->id,
            [
            'amount' => $full_intent_amount,
            'currency' => 'eur',
            'payment_method_types' => ['card','sofort'],
            'metadata' => [
              'stripe' => $accounts,
              'products' => implode('###',$products)
            ]  

          ]);
        endif;


      $orderitems = $this->getOrderItems();
      $_SESSION['orderitems'] = $orderitems;

       if($echo): echo "intent created";  endif;
        die();
      else:
        echo "no products on list";
        die();
      endif;
    else:
        echo "no online products on list";
        die();
    endif;
  }
  
  
  
  */