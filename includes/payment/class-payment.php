<?php
namespace HalloHalle_Onlineshop\Payment;
use HalloHalle_Onlineshop\Payment\Stripe as Stripe;
use HalloHalle_Onlineshop\Cart\Session as Session;
use HalloHalle_Onlineshop\Product\Product as Product; 
use HalloHalle_Onlineshop\Content\Post as Post; 
use HalloHalle_Onlineshop\Order\Order as Order;
use HalloHalle_Onlineshop\Invoices\Invoice as Invoice;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Payment Factory
 *
 *
 */
class Payment {

    public $payment_provider;

    public $payment_object;

    public $order;

    public $session;

    public $payment_success_url = 'http://localhost/demoshop/erfolg/'; //should be an option 

    public $payment_error_url = 'http://localhost/demoshop/erroer/'; //should be an option 

    public function __construct(Stripe $pp, Session $session){
        add_action('checkout_debug', array($this, 'checkoutDebug'));
        add_action('prepare_payment', array($this, 'preparePayment'));
        add_action('handle_payment', array($this, 'handlePayment'));
        add_action('handle_order', array($this, 'handleOrder'));

        $this->payment_provider = $pp;
        $this->session = $session;
    }


    public function preparePayment(){

        $this->payment_provider->preparePayment();
    }

    /** invoked after beeing returned from payment_page. */
    public function handlePayment(){
        if($this->session->getVar('seller')):
         //   $status = sanitize_text_field($_REQUEST['redirect_status']);
        

            $this->session->setConcreteProducts(); 
            
            //create order object
            $orderobject = Order::getOrderObject();
            $this->order = Post::createPost($orderobject,'order');
            $this->session->setVar('order_id', $this->order);

            $invoices = Invoice::addInvoices();
            Order::updateOrderInvoices(serialize($invoices),$this->session->getVar('order_id'));
            Order::updateOrderSellers(serialize($this->session->getVar('seller')),$this->session->getVar('order_id'));

            // if($status === 'succeeded') {
            //     Order::updateOrderStatus('paid',$this->session->getVar('order_id'));
            //     wp_redirect(get_bloginfo('url').'/erfolg?payment_succeeded=1');
            // } else {
            //     Order::updateOrderStatus('unpaid',$this->session->getVar('order_id'));
            // }  
         
            return get_bloginfo('url').'/payment/?order_id='.$this->session->getVar('order_id');
        endif;    
    }



    public function handleOrder(){
        if(!is_numeric($_REQUEST['order_id'])):
            return;
        endif;
        $status = sanitize_text_field($_REQUEST['redirect_status']);
        if($status === 'succeeded') {
                echo _e('<p class="alert alert-success">Ihre Bestellung wurde erfolgreich ausgeführt.</p>','hallohalle_onlineshop');
                Order::updateOrderStatus('paid',$_REQUEST['order_id']);
        } else {
            echo _e('<p class="alert alert-error">Ihre Bestellung konnte nicht ausgeführt werden.</p>','hallohalle_onlineshop');
                Order::updateOrderStatus('unpaid',$_REQUEST['order_id']);
        }
        $this->session->deleteSession();
        
    }


    /** adds a payment object on checkout page 
    * @return object
    */
    public function addPaymentObject(){
        /** get payment data (Payment intent from stripe) */
        $this->payment_object = $this->payment_provider->getPaymentObject();
    }


    public static function updateSellerTotals($sellers_object){
        foreach($sellers_object as $seller){
            $sellers_object['seller_'.$seller['id']]['total'] = self::getPaymentTotal($sellers_object['seller_'.$seller['id']]['products']);
        }
       
        return $sellers_object;
    }



    public static function getPaymentTotal($products){
        $total = 0;
      
        foreach($products as $product){
            $price = Product::getProductPrice($product['product_id']) * $product['product_count'];
            $total += $price;
        }

        return $total;
    }

}