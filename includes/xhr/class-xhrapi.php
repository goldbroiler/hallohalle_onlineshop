<?php
namespace HalloHalle_Onlineshop\Xhr;
use HalloHalle_Onlineshop\Cart\Cart as Cart;
use HalloHalle_Onlineshop\Cart\Session as Session;
use HalloHalle_Onlineshop\Payment\Stripe as Stripe;
use HalloHalle_Onlineshop\Payment\Payment as Payment;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * XhrApi
 *
 * offers api to shop 
 */
class XhrApi {

    public $cart;

    private $stripe;

    private $session;

    private $payment;

    public function __construct(Cart $cart, Stripe $stripe, Session $session, Payment $payment){
        $this->cart = $cart;
        $this->stripe = $stripe;
        $this->session = $session;
        $this->payment = $payment;
    }


    public function addProduct(){
        echo 'haha';
        echo $this->cart->addProduct();
        die();
    }

    public function removeProduct(){
        $this->cart->removeProduct();
    }
    
    public function getItemCount(){
        echo $this->cart->getItemCount();
        die();
    }

    public function getCartTotal(){
        echo $this->cart->getTotal();
        die();
    }

    public function getPreparedPaymentId(){
        echo $this->stripe->getPreparedPaymentId();
        die();
    }


    public function deleteSession(){
        echo $this->session->deleteSession();
        die();
    }

    public function getOptions(){
        $options['return_url'] = 'http://localhost/demoshop/payment';
        echo json_encode($options);
        die();
    }
 
    public function handlePayment(){
        echo $this->payment->handlePayment();
        die();
    }
}
